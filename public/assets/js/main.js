'use strict';

//Text area animation 
$.fn.textWidth = function (text, font) {

    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

    $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

    return $.fn.textWidth.fakeEl.width();
};

$('.width-dynamic').on('input', function () {
    var inputWidth = $(this).textWidth();
    $(this).css({
        width: inputWidth
    });
}).trigger('input');

function inputWidth(elem, minW, maxW) {
    elem = $(this);
    console.log(elem);
}

var targetElem = $('.width-dynamic');

inputWidth(targetElem);

//Input Analysis
$("#text").on("submit", function (event) {
    event.preventDefault();
    var submitText = $("#input").val();

    // Count cowels
    function countVowels(text) {
        // Search text with Regex
        var textInput = text.match(/[aeiou]/gi);
        // Check if the text input exist then calculate length
        if (textInput) {
            // Log formatted textInput to console 
            console.log('The text contains ' + textInput.length + ' vowel(s)');
            $("#result").append('<p>The text contains ' + textInput.length + ' vowel(s)</p>');
            // Return number of vowels
            return textInput.length;
        } else {
            return 0;
        }
    }

    //Count consonants
    function countConsonants(text) {
        // Search text with Regex
        var textInput = text.match(/[bcdfghjklmnpqrstwxz]/gi);
        // Check if text input exist then calculate length
        if (textInput) {
            // Log formatted textInput to console 
            console.log('The text contains ' + textInput.length + ' consonants(s)');
            $("#result").append('<p>The text contains ' + textInput.length + ' vowel(s)</p>');
            // Return number of vowels
            return textInput.length;
        } else {
            return 0;
        }
    }

    countVowels(submitText);
    countConsonants(submitText);
});
